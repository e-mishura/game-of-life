case class Position(x: Int, y: Int) {
  def +(o: Offset): Position = Position(x + o.dx, y + o.dy)
}
case class Offset(dx: Int, dy: Int)
case class Boundary(minX: Int, minY: Int, maxX: Int, maxY: Int) {

  def +(p: Position): Boundary = Boundary(
    Math.min(p.x, minX), Math.min(p.y, minY),
    Math.max(p.x, maxX), Math.max(p.y, maxY),
  )

  def +(b: Boundary): Boundary = Boundary(
    Math.min(b.minX, minX), Math.min(b.minY, minY),
    Math.max(b.maxX, maxX), Math.max(b.maxY, maxY),
  )

  def enlarge(delta: Int = 1): Boundary = Boundary(minX-delta, minY-delta, maxX+delta, maxY+delta)
}

object Boundary {
  val empty = Boundary(Int.MaxValue, Int.MaxValue, Int.MinValue, Int.MinValue)
  def apply(p: Position): Boundary = this(p.x, p.y, p.x, p.y)
}

object GameOfLife extends App {


  //THIS IS TEST RUN

  printEvolution(glider, 17)

//  printEvolution(pulsar, 5)

//  evolution(glider).drop(1000000).take(4)
//    .flatMap(b =>   printBoard(b, boundary(b)) :+ "")
//    .foreach(println)

  type Board = Set[Position]

  def printEvolution(seed: Board, steps: Int) : Unit = {
    val ev = evolution(seed).take(steps)
    val bb = ev.map(boundary).reduce(_ + _)
    ev.flatMap(turn => printBoard(turn, bb) :+ "")
      .foreach(println)
  }

  /*
       -----> x
       |
       |
       y
  */
  def printBoard(board: Board, boundary: Boundary): Seq[String] =
    for {
      y     <-  boundary.minY to boundary.maxY
      lineChars  =  for {
        x <- boundary.minX to boundary.maxX
      }
      yield if (board.contains(Position(x, y))) 'X' else '.'
    }
    yield lineChars.mkString(" ")


  def evolution(seed: Board): Stream[Board] = Stream(seed) #::: evolution(nextTurn(seed))

  def boundary(b: Board): Boundary = b.foldLeft(Boundary.empty)(_ + _)

//  val bb = Set(Position(1, 1), Position(1, 2))
//  println(boundary(bb))

  lazy val neighbourOffsets: Seq[Offset] = (
    for {
      dx <- -1 to 1
      dy <- -1 to 1
    } yield Offset(dx, dy)
  ).filter(o => !(o.dx == 0 && o.dy == 0))

  //println(neighbourOffsets)

  def countNeighbours(p: Position, b: Board): Int = neighbourOffsets
    .map { offset => p + offset }  //neighbour position
    .map { n => if(b.contains(n)) 1 else 0 }
    .sum

//  println(countNeighbours(Position(1, 1), glider)) //should be 5
//  println(countNeighbours(Position(0, 1), glider)) //should be 3

  def nextTurn(board: Board) : Board = {
    val testBoundary = boundary(board).enlarge(1)
    val positions = for {
      x <- testBoundary.minX to testBoundary.maxX
      y <- testBoundary.minY to testBoundary.maxY
    } yield Position(x, y)

    positions
      .flatMap { p => nextCellState(p, board) }
      .toSet
  }

  /**
    * @return Some(Position) if next state is alive. None if next state is dead
    */
  def nextCellState(p: Position, board: Board) : Option[Position] = countNeighbours(p, board) match {
    case n if n < 2 => None
      //unchanged
    case n if n == 2 => if(board.contains(p)) Some(p) else None
    case n if n == 3 => Some(p)
    case _ => None
  }

  /*
    . X .
    . . X
    X X X
   */
  def glider : Board = Set(
      Position(1, 0),
        Position(2, 1),
    Position(0, 2), Position(1, 2), Position(2, 2),
  )

  /*
      . . . . X . . . .
      . . . . X . . . .
      . . . . X . . . .
      . . . . . . . . .
      X X X . . . X X X
      . . . . . . . . .
      . . . . X . . . .
      . . . . X . . . .
      . . . . X . . . .
   */
  def pulsar: Board = Set (
    Position(0, 4),
    Position(1, 4),
    Position(2, 4),
    Position(4, 0), Position(4, 1), Position(4, 2), Position(4, 6), Position(4, 7), Position(4, 8),
    Position(6, 4),
    Position(7, 4),
    Position(8, 4),
  )

}


